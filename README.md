# Quad Max31856
4 sets of MAX31856 built on board

(c) 2016-2019 [James Hwang](mailto:james.hwang@sylmac.cc) / [Sylmac Labs](http://www.sylmac.cc)


# Hardware Notes
1.  4 sets of MAX31856 thermocouple converter
2.  Works at 3.3V or 5V
3.  PCC-SMP style mini jack

# Hardware Connections to Arduino UNO
1. VIN = 5V or 3.3V
2. GND = Ground
3. 13 --> SCLK
4. 12 --> MISO
5. 11 --> MOSI
6. 10 --> TC0
7.  9 --> TC1
8.  8 --> TC2
8.  7 --> TC3

# Software Notes
1.  Requires Adafruit MAX31856 library



